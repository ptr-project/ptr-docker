#!/bin/sh

# Copyright (c) 2019 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

set -eux

SCRIPT_PATH=$(dirname "$0")

buildImage() {
  folder=$1
  docker build -t "${CI_REGISTRY_IMAGE}/${folder}:${IMAGE_VERSION}" \
    --build-arg=IMAGE_VERSION --build-arg=CI_REGISTRY_IMAGE \
    "${folder}"
  docker push "${CI_REGISTRY_IMAGE}/${folder}:${IMAGE_VERSION}"
}

promoteImage() {
  folder=$1
  docker pull "${CI_REGISTRY_IMAGE}/${folder}:${IMAGE_VERSION}"
  docker tag "${CI_REGISTRY_IMAGE}/${folder}:${IMAGE_VERSION}" "${CI_REGISTRY_IMAGE}/${folder}:latest"
  docker push "${CI_REGISTRY_IMAGE}/${folder}:latest"
}

if [ -z "${IMAGE_VERSION+x}" ]; then
  echo Missing IMAGE_VERSION env variable;
  exit 1
fi

if [ -z "${CI_REGISTRY_IMAGE+x}" ]; then
  echo Missing CI_REGISTRY_IMAGE env variable;
  echo This variable has to contain the full path to the registry
  echo E.g., registry.gitlab.com/kaihowl/transport-routing
  exit 1
fi

cd "$SCRIPT_PATH"

if [ $# -ne 1 ]; then
    echo Missing parameter: build / promote
    exit 1
fi

if [ $1 == 'build' ]; then
  buildImage build-base
  buildImage build-gcc
  buildImage build-clang
elif [ $1 == 'promote' ]; then
  promoteImage build-base
  promoteImage build-gcc
  promoteImage build-clang
else
  echo Unknown command $1
  exit 1
fi

